from xml.dom import minidom
import os

dictionary = {}

def string_handler(permission):
    dot_count = permission.count(".")
    permission_set = permission.split(".", dot_count)
    return permission_set[dot_count]

def find_unique(permission, key, dictionary):
    for perm_list_key in dictionary:
        if(perm_list_key != key):
            for permission_comparator in dictionary[perm_list_key]:
                if(permission_comparator == permission):
                    return 1
    return 0

def print_unique(dictionary):
    print("")
    print("===================")
    print("")
    print("Permissões únicas por APK")
    print("")
    print("===================")
    print("")

    for x in dictionary: #permission list
        for permission in dictionary[x]:
            unique_perm_list = []
            if(find_unique(permission, x, dictionary) != 1):             
                unique_perm_list.append(permission)            
        print(x, ":", unique_perm_list)

def find_commom(permission, key, dictionary):
    find = 0
    for perm_list_key in dictionary:
        if(perm_list_key != key):
            for permission_comparator in dictionary[perm_list_key]:
                if(permission_comparator == permission):
                    find = 1
                    break
                else:
                    find = 0
        if(find == 0):
            return 0
    return 1

def print_common(dictionary):
    print("")
    print("===================")
    print("")
    print("Permissões comuns das APKs")
    print("")
    print("===================")
    print("")

    common_perm_list = []
    for x in dictionary: #permission list
        for permission in dictionary[x]:
            find = 0
            if(find_commom(permission, x, dictionary) == 1):             
                for k in common_perm_list:
                    if(k == permission):
                        find = 1
                if(find != 1):
                    common_perm_list.append(permission)
    print(common_perm_list)

def print_all(dictionary):
    print("")
    print("===================")
    print("")
    print("Permissões por APK")
    print("")
    print("===================")
    print("")
    for x in dictionary:
        print(x, ": ", dictionary[x]) 

for apkFileName in os.listdir("manifests"):
    apkName = minidom.parse('manifests/' + apkFileName).getElementsByTagName('manifest')[0].attributes['package'].value
    permissions = minidom.parse('manifests/' + apkFileName).getElementsByTagName('uses-permission')

    #handle the permission strings
    permissionsList = []
    for permission in permissions:
        permissionsList.append(string_handler(permission.attributes['android:name'].value))

    dictionary[apkName] = permissionsList

print_all(dictionary)
print_unique(dictionary)
print_common(dictionary)
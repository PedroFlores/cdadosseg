import sys
import os
import pefile

def common_fields(file1, file2):
    sections1 = []
    sections2 = []
   
    for section in file1.sections:
        sections1.append(section.Name.decode('utf-8'))

    for section in file2.sections:
        sections2.append(section.Name.decode('utf-8'))

    common = [x for x in sections1 if x in sections2]
    
    return common

def unique_fields(former, latter): 
    former_sections = []
    latter_sections = []

    for section in former.sections:
       former_sections.append(section.Name.decode('utf-8'))

    for section in latter.sections:
        latter_sections.append(section.Name.decode('utf-8'))

    unique = [x for x in former_sections if x not in latter_sections]
    return unique

dictionary = {}
File1Fields = []
File2Fields = []
commonFields = []

file1 = pefile.PE('executables/' + sys.argv[1])
file2 = pefile.PE('executables/' + sys.argv[2])

commonFields = common_fields(file1, file2)
File1Fields = unique_fields(file1, file2)
File2Fields = unique_fields(file2, file1)

dictionary["secoes_comuns"] = commonFields
dictionary["exclusivo_arquivo_1"] = File1Fields
dictionary["exclusivo_arquivo_2"] = File2Fields

for x in dictionary:
    print(x, ":", dictionary[x])

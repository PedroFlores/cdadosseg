import sys
import os
import pefile

dictionary = {}

for filename in os.listdir("executables/"):
    pe = pefile.PE('executables/' + filename)

    for section in pe.sections:
        executablesSections = []
        #check the IMAGE_SCN_CNT_CODE flag value        
        if (hex(section.Characteristics)[8] == '2'):
            executablesSections.append(section.Name.decode('utf-8'))
            dictionary.update({filename: executablesSections})  

for x in dictionary:
    print(x, ": ", dictionary[x]) 